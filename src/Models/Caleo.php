<?php
declare(strict_types=1);

namespace HybrideLabs\Caleo\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Caleo Events
 *
 * @package HybrideLabs\Caleo\Models
 */
class Caleo extends Model
{
    /** @var string|null $table */
    protected $table;

    /** @inheritDoc */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Caleo events constructor.
     *
     * @param  array  $attributes
     */
    public function __construct(array $attributes = [])
    {
        if ( ! isset($this->table)) {
            /** @var string $table */
            $table = config('caleo.database.tables.events');
            $this->setTable($table);
        }

        parent::__construct($attributes);
    }
}
