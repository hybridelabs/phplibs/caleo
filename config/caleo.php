<?php
declare(strict_types=1);

return [
    /** Database related configuration. */
    'database' => [
        /** Table names */
        'tables' => [
            'events'       => 'caleo_events',
            'entries'      => 'caleo_entries',
            'participants' => 'caleo_participants',
        ],
    ],

];
