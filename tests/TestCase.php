<?php
declare(strict_types=1);

namespace HybrideLabs\Caleo\Tests;

use HybrideLabs\Caleo\CaleoServiceProvider;
use Orchestra\Testbench\TestCase as TestCaseBase;

class TestCase extends TestCaseBase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app)
    {
        return [
            CaleoServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetup($app)
    {
        include_once __DIR__ . '/../database/migrations/create_users_table.php.stub';
        include_once __DIR__.'/../database/migrations/create_events_table.php.stub';

        (new \CreateUsersTable())->up();
        (new \CreateEventsTable())->up();
    }
}
