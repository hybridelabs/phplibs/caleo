<?php
declare(strict_types=1);

namespace HybrideLabs\Caleo;

use Illuminate\Support\ServiceProvider;

class CaleoServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/caleo.php', 'caleo');
    }

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes(
                [
                    __DIR__.'/../config/caleo.php' => config_path('caleo.php'),
                ],
                'config'
            );
        }
    }
}
